Madame, Monsieur,

Les citoyens ont récemment appris par la presse qu'en dépit d'une requête des parlementaires ayant initié le Référendum d’initiative partagée (RIP) sur Aéroports de Paris, le CSA n'inciterait pas les chaînes de télévision à traiter le sujet.

C'est très regrettable car la majorité des Français ignore qu'une campagne est en cours. On s'en rend compte lorsqu'on va au devant des gens. Ils ne sont pas informés mais lorsqu'on les informe, ils sont intéressés.

Lorsque le président de la République a initié le "grand débat" pour répondre au mouvement des "Gilets jaunes", ce grand débat a été abondamment médiatisé. Il ne s'agissait pourtant pas d'un dispositif prévu par la Constitution. En revanche, le RIP l'est. D'ailleurs, le recueil des signatures de soutien se fait sur un site officiel, sous contrôle de Conseil constitutionnel. Ce n'est pas une simple pétition.

Certes, la loi organique relative au RIP ne prévoit rien de spécial en termes de médiatisation. Mais la loi ne prévoit pas toujours tout, et le silence des textes n'interdit pas le bon sens. Un processus électoral (c'en est un puisque l'idée est d'aboutir à l'organisation d'un référendum), cela se médiatise.

C'est d'autant plus simple qu'il ne s'agit nullement d'inciter le public à accepter ou refuser la privatisation. Les signatures qui sont recueillies actuellement visent à soutenir le principe d'une consultation électorale, sans préjuger de son résultat. A ce stade, ce n'est qu'une question de démocratie, et il est tout à fait possible de la traiter sous cet angle uniquement, de manière très neutre.

Enfin, le CSA a répondu aux députés venus à sa rencontre qu'il n'avait pas la possibilité de contraindre les chaînes à traiter tel ou tel sujet. Et bien, ne contraignez pas. Incitez, ce sera déjà beaucoup. Les télévisions ont-elle beaucoup mieux à faire en période estivale ? Le sujet peut utilement meubler des journaux ou des émissions, entre deux salves de commentaires répétitifs sur la canicule.

Merci par avance pour l'intérêt que vous voudrez bien porter à ma requête.
