$(document).ready(function() {
    var villes = null
    var departements = null
    var currentDataTs = null
    var palmaresDepartementsTable
    var palmaresCommunesTable
    var page = parseInt(url.searchParams.get('p'), 10) || 0
    var pageLength = parseInt(url.searchParams.get('pl'), 10) || 10

    $('#header').load(baseUrl + 'navigation.html')
    $('#stats_disclaimer').load(baseUrl + 'stats_disclaimer.html')

    function formatArrondissements(nom) {
        return nom.replace(/([a-zA-Z]+)([0-9]+)(er?)Arrondissement/, '$1&nbsp;$2$3&nbsp;Arrondissement')
            .replace(/-/g, '&#8209;')
    }
    function shouldHideDpt(deptCode) {
                //TAAFs                Clipperton          Autres
        return deptCode == "984" || deptCode == "989" || deptCode == "999"
    }

    // Replace browser url to ease page sharing
    function refreshURL() {
        if (!window.history.replaceState) {
            return
        }
        var params = new URLSearchParams(),
            param_dpt_q = $('#PalmaresDepartements_filter input').val(),
            param_com_q = $('#PalmaresCommunes_filter input').val(),
            param_dpt = $('#departementFilter').val(),
            param_arr = $('#arrondissementFilter').val(),
            param_min = $('#minElecteurs').val(),
            param_max = $('#maxElecteurs').val(),
            pageInfo

        if ($('#pills-communes-tab').hasClass('active')) {
            pageInfo = palmaresCommunesTable.page.info()
            params.set('vue', 'com')
            if (param_com_q) params.set('q', param_com_q)
            if (param_dpt) params.set('dpt', param_dpt)
            if (param_arr === '0') params.set('arr', param_arr)
            if (param_min) params.set('min', param_min)
            if (param_max) params.set('max', param_max)
        } else {
            pageInfo = palmaresDepartementsTable.page.info()
            params.set('vue', 'dept')
            if (param_dpt_q) params.set('q', param_dpt_q)
        }
        if (pageInfo.page > 0) params.set('p', pageInfo.page)
        if (pageInfo.length != 10) params.set('pl', pageInfo.length)

        if (url.protocol === 'https:') {
            window.history.replaceState({}, null, window.location.pathname + '?' + params)
        }
    }

    // Palmares des departements
    function tableDepartements() {
        var dptFilter = $('#departementFilter')
        var DPTs = []
        for(var i = 0; i <= 19; i++) {
                DPTs.push(i.toString(10).padStart(2, '0'))
        }
        DPTs = DPTs.concat(["2A", "2B"])
        for(var i = 21; i <= 95; i++) {
                DPTs.push(i.toString(10))
        }
        DPTs = DPTs.concat(["971", "972", "973", "974", "975", "976", "977", "978", "986", "987", "988"])

        // Re-init departement dropdown
        dptFilter.empty()
        dptFilter.append('<option value="">Tous les d&eacute;partements</option>')
        dptFilter.append('<option disabled="disabled">───────────────────────────</option>')

        $.each(DPTs, function(i, dpt) {
            departements[dpt].nom = formatDepartements(departements[dpt].nom)
            dptFilter.append(
                $('<option>', { value: dpt, text: dpt + ' - ' + departements[dpt].nom})
            )
            departements[dpt].nom = departements[dpt].nom.replace(/-/g, '&#8209;') //utiliser tirets insecables pour le tableau
        })

        // Select departement from url
        var urlDepartement = url.searchParams.get('dpt')
        if ($('#departementFilter option[value="' + urlDepartement + '"]').length > 0) {
            $('#departementFilter').val(urlDepartement)
        }

        var dataForTable = $.map(departements, function(deptObj, deptCode) {
            var pourcent = 0
            if (deptObj.soutiens > 0 && deptObj.soutiens < deptObj.electeurs) {
                pourcent = 100.0 * deptObj.soutiens / deptObj.electeurs
            }
            return [[
                deptCode,
                deptObj.nom,
                pourcent,
                deptObj.soutiens,
                deptObj.electeurs
            ]]
        })
        .sort(function(d1, d2) {
            return d2[2] - d1[2]
        })
        .map(function(item, index) {
            item.unshift(index + 1)
            item[3] = item[3].toFixed(2)
            return item
        })

        if (palmaresDepartementsTable) {
            palmaresDepartementsTable.clear()
            palmaresDepartementsTable.rows.add(dataForTable)
            palmaresDepartementsTable.draw()
        } else {
            palmaresDepartementsTable = $('#PalmaresDepartements').DataTable({
                data: dataForTable,
                language: {
                    url: baseUrl + 'js/datatable_french.json'
                },
                deferRender: true,
                searchDelay: 500,
                search: {
                    search: url.searchParams.get('q') || ''
                },
                displayStart: page * pageLength,
                pageLength: pageLength,
                columns: [
                    {
                        title: '',
                        className: 'text-right small font-weight-bold',
                        render: $.fn.dataTable.render.number('&nbsp;', ',', 0, '', '&nbsp;e')
                    },
                    {
                        title: 'Code',
                        className: 'text-right'
                    },
                    {
                        title: 'D&eacute;partement'
                    },
                    {
                        title: 'Pourcentage',
                        render: $.fn.dataTable.render.number('&nbsp;', ',', 2, '', '&nbsp;%'),
                        className: 'text-right',
                    },
                    {
                        title: 'Soutiens',
                        render: $.fn.dataTable.render.number('&nbsp;', ',', 0),
                        className: 'text-right'
                    },
                    {
                        title: "Nombre d'&eacute;lecteurs",
                        render: $.fn.dataTable.render.number('&nbsp;', ',', 0),
                        className: 'text-right',
                    },
                ],
                order: [
                    [
                        3,
                        'desc',
                    ],
                ],
            })
            palmaresDepartementsTable.on('draw', refreshURL)
        }
    }

    //~ // Palmares des villes
    function tableVilles() {
        var dataForTable = $.map(villes, (function(ville, codeInsee) {
            var pourcent = 0
            if (ville.electeurs > 0 && ville.soutiens < ville.electeurs) {
                pourcent = 100.0 * ville.soutiens / ville.electeurs
            }
            var departement = codeInsee.substr(0, 2)
            if (departement === '97' || departement === '98') {
                departement = codeInsee.substr(0, 3)
            }
            return [[
                departement,
                codeInsee,
                formatArrondissements(ville.nom),
                pourcent,
                ville.soutiens,
                ville.electeurs,
                ville.homonymes.length,
            ]]
        }))
        .sort(function(c1, c2) {
            return c2[3] - c1[3]
        })
        .map(function(item, index) {
            item.unshift(index + 1)
            item[4] = item[4].toFixed(2)
            return item
        })

        if (palmaresCommunesTable) {
            palmaresCommunesTable.clear()
            palmaresCommunesTable.rows.add(dataForTable)
            palmaresCommunesTable.draw()
            $('#loadingBox').hide()
        } else {
            palmaresCommunesTable = $('#PalmaresCommunes').DataTable({
                data: dataForTable,
                language: {
                    url: baseUrl + 'js/datatable_french.json'
                },
                deferRender: true,
                searchDelay: 500,
                search: {
                    search: url.searchParams.get('q') || ''
                },
                displayStart: page * pageLength,
                pageLength: pageLength,
                columns: [
                    {
                        title: '',
                        className: 'text-right small font-weight-bold',
                        render: $.fn.dataTable.render.number('&nbsp;', ',', 0, '', '&nbsp;e')
                    },
                    {
                        title: 'D&eacute;pt.',
                        className: 'text-right'
                    },
                    {
                        title: 'Insee',
                        className: 'text-right',
                        visible: false
                    },
                    {
                        title: 'Commune',
                        render: function (data, type, row) {
                            return data + (row[7] == 0 ? '' :
                                '<span style="cursor: pointer" title="Voir note en bas de page">*</span>')
                        }
                    },
                    {
                        title: 'Pourcentage',
                        render: $.fn.dataTable.render.number('&nbsp;', ',', 2, '', '&nbsp;%'),
                        className: 'text-right',
                    },
                    {
                        title: 'Soutiens', render: $.fn.dataTable.render.number('&nbsp;', ',', 0),
                        className: 'text-right'
                    },
                    {
                        title: "Nombre d'&eacute;lecteurs",
                        render: $.fn.dataTable.render.number('&nbsp;', ',', 0),
                        className: 'text-right',
                    },
                ],
                order: [
                    [
                        4,
                        'desc',
                    ],
                ],
                initComplete: function(settings, json) {
                    $('#loadingBox').hide()
                    if (url.hash === '#communes' || 'com' === url.searchParams.get('vue')) {
                        $('#pills-communes-tab').trigger('click')
                    }
                },
            })
            palmaresCommunesTable.on('draw', refreshURL)
        }
    }

    function reload(newDate) {
        // Load departement/commune data using cache from utils.js
        reloadDataTS(newDate, function(data_departements, data_communes) {
            $.each(data_departements, function(deptCode) {
                if (shouldHideDpt(deptCode)) {
                    delete data_departements[deptCode]
                }
            })
            departements = data_departements
            villes = data_communes

            tableDepartements()
            tableVilles()
        })
    }

    // Get a list of available data timestamps
    $.get(urlADPRipCarte , function(data) {
        currentDataTs = fillDataDateSelect(data, $('select#dataDate'))
    }).then(function() {
        reload(currentDataTs)
    })

    $('select#dataDate').on('change', function () {
        if (this.value != $(this).data("prev")) {
            $('#loadingBox').show()
            reload(this.value)
        }
    })

    //~ // Search sans accents
    $.fn.dataTableExt.ofnSearch['string'] = function(data) {
        return !data
            ? ''
            : typeof data === 'string'
              ? data
                    .replace(/\n/g, ' ')
                    .replace(/[áâàä]/g, 'a')
                    .replace(/[éêèë]/g, 'e')
                    .replace(/[íîï]/g, 'i')
                    .replace(/[óôö]/g, 'o')
                    .replace(/[úüù]/g, 'u')
                    .replace(/[ÿ]/g, 'y')
                    .replace(/ñ/g, 'n')
                    .replace(/æ/g, 'ae')
                    .replace(/œ/g, 'oe')
                    .replace(/ç/g, 'c')
                    .replace(/&#8209;/g, '-')
              : data
    }

    // Filtres
    $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
        // Pas de filtre pour la vue par departement
        if (settings.sTableId === 'PalmaresDepartements') {
            return true
        }
        var dptFilter = $('#departementFilter').val()
        var arrFilter = $('#arrondissementFilter').val()
        var min = parseInt($('#minElecteurs').val(), 10)
        var max = parseInt($('#maxElecteurs').val(), 10)
        var dataDpt = data[1]
        var dataVille = data[3]
        var dataElecteurs = parseInt(data[6]) || 0

        if (
            (isNaN(min) || dataElecteurs >= min) &&
            (isNaN(max) || dataElecteurs <= max) &&
            (!dptFilter || dataDpt === dptFilter) &&
            (arrFilter === '1' || !dataVille.includes('Arrondissement'))
        ) {
            return true
        }
        return false
    })

    // Tab change event
    $('#pills-communes-tab, #pills-dpt-tab').on('shown.bs.tab', refreshURL)

    // Populate filters
    $('#minElecteurs').val(parseInt(url.searchParams.get('min'), 10) || '')
    $('#maxElecteurs').val(parseInt(url.searchParams.get('max'), 10) || '')
    $('#arrondissementFilter').val(url.searchParams.get('arr') === '0' ? 0 : 1)

    // Event listener to the two range filtering inputs to redraw on input
    $('#minElecteurs, #maxElecteurs').keyup(
        _.debounce(function() {
            palmaresCommunesTable.draw()
        }, 300)
    )
    $('#departementFilter, #arrondissementFilter').change(
        _.debounce(
            function() {
                palmaresCommunesTable.draw()
            },
            300,
            { leading: true }
        )
    )
    $('button.toggle-vis').on('click', function (e) {
        e.preventDefault()
        // Get the column API object
        var column = palmaresCommunesTable.column($(this).attr('data-column'))
        // Change button text
        if (column.visible()) {
            $(this).text($(this).text().replace('Masquer', 'Afficher'))
        } else {
            $(this).text($(this).text().replace('Afficher', 'Masquer'))
        }
        // Toggle the visibility
        column.visible(!column.visible())
    })
})
