'use strict';

const isIos = /ip(hone)/i.test(navigator.userAgent);
const isIpad = /ip(ad)/i.test(navigator.userAgent);
const isAndroid = /android/i.test(navigator.userAgent);
const isMobile = isIos || isAndroid;

const testTablet = /Tablet|iPad/i.test(navigator.userAgent);
const isTablet = (isMobile && testTablet || isIpad);

// Nombre total de soutiens requis (4.7 Million)
const requiredTotal = 4717396;

// calcul théorique pour estimer les 10 % du corps électoral nécessaire
// soit ~ 4 717 396 personnes / 274 jours (du 13 juin 2019 au 13 mars 2020)
// moyenne journalière à tenir d'environ 17 217 soutiens...
const perDay_required = 17217;

// Date d'aujourd'hui
var today = moment().startOf("days");

const dateStart = moment("2019-06-13");
const dateEnd = moment("2020-03-12");

$(() => {
    $('#header').load('navigation.html');
    onStart();
    setIntro();
});

$.get('./archive/history.dat?nocache', function (result) {
    $.csv.toArrays(result, {
        "separator": ";"
    }, function (err, rawData) {
        $.get('./archive/latest.count.json?nocache', function (result) {
            // ajouté au cas où history.dat n'est pas mis à jour sur la journée seulement... à revoir le cas échéant
            if (+rawData[rawData.length - 1][1] !== result["total"])
                rawData.push([Math.floor(Date.now() / 1000), result["total"]]);

            var mappedData = rawData.map(item => [
                moment(+item[0] * 1000).format("YYYY-MM-DD"),
                parseInt(item[1])
            ]);
            // console.log(mappedData);
            var data = mappedData.filter(function (item, pos) {
                if (pos === mappedData.length - 1) {
                    return true;
                } else {
                    // Keep only latest timestamp of each day
                    return (mappedData[pos + 1][0] !== item[0])
                }
            });

            // Lissage sur les 5 premiers jours des 26 157 soutiens enregistré en une seule fois à la date du 18/06/2019
            var joursDecalage = [
                ["2019-06-13", 21026],
                ["2019-06-14", 42452],
                ["2019-06-15", 63078],
                ["2019-06-16", 84104],
                ["2019-06-17", 105130]
            ];
            data = joursDecalage.concat(data);

            // console.log(data);

            var config = generateConfig(data, rawData);


            if (err) {
                console.log(err);
                return;
            }

            drawGauge(config);
            $('select#selectCharts').prop('selectedIndex', getUrlParameter('g'));
            $('select#selectCharts').trigger('change');
            $("#mainloader").hide();
        })
    })
})

function generateConfig(data, rawData, prevision) {
    const timestamp = +rawData[rawData.length - 1][0];

    const lastUpdate = timestamp * 1000;
    const latestCount = data[data.length - 1][1];

    var updateCheck = (moment(lastUpdate).format("YYYY-MM-DD") !== today.format("YYYY-MM-DD"));

    const days_complete = dateEnd.diff(dateStart, 'days') + 1;
    const days_done = (updateCheck) ? today.diff(dateStart, 'days') : today.diff(dateStart, 'days') + 1;
    const days_remaining = days_complete - days_done; //dateEnd.diff(today, 'days') + 1; //

    var chartData = [];
    var cumul = 0,
        days = 1,
        lastMinimale,
        lastMoyenne;

    for (var i = 0, lgi = days_done; i < lgi; i++) {
        var dateStr = moment('2019-06-13').add(i, 'days').format("YYYY-MM-DD"),
            visits = 0,
            difference,
            moyenne_mobile,
            moyenne_minimale;

        cumul += perDay_required;

        data.forEach((arr, i) => {
            if (dateStr === arr[0])
                visits = +arr[1];

            difference = visits - cumul;
            moyenne_mobile = Math.round(visits / days);
            moyenne_minimale = (moyenne_mobile === 0) ? perDay_required :
                (moyenne_mobile >= perDay_required) ? perDay_required - (moyenne_mobile - perDay_required) :
                Math.round(perDay_required + (Math.abs(difference) / days_remaining));
            if (i === data.length - 1) {
                lastMinimale = moyenne_minimale;
                lastMoyenne = moyenne_mobile;
            }
        });

        chartData.push({
            date: dateStr,
            visits: visits,
            cumul_theorique: cumul,
            difference: difference,
            moyenne_mobile: moyenne_mobile,
            moyenne_minimale: moyenne_minimale
        });

        // visits === 0 n'est pas une condition à ajouter ici car il peut y avoir une ou plusierus journées sans aucun soutien... ?
        chartData[i]['evolution'] = (i > 0) ? chartData[i]["visits"] - chartData[i - 1]["visits"] : chartData[i]["visits"];
        days++;
    }

    var dataSerie_officiel = [{
            date: "2019-07-01",
            comptage_off: 465900
        },
        {
            date: "2019-07-31",
            comptage_off: 597000
        }
    ];

    // calculs des différentes moyennes
    var average_last30Days = chartData.slice(chartData.length - 30, chartData.length)
        .map(obj => obj["evolution"]).reduce((a, b) => a += b);
    average_last30Days = Math.round(average_last30Days / 30);

    var average_last14Days = chartData.slice(chartData.length - 14, chartData.length)
        .map(obj => obj["evolution"]).reduce((a, b) => a += b);
    average_last14Days = Math.ceil(average_last14Days / 14);

    var average_last7Days = chartData.slice(chartData.length - 7, chartData.length)
        .map(obj => obj["evolution"]).reduce((a, b) => a += b);
    average_last7Days = Math.round(average_last7Days / 7);

    var averagePerDay_sinceStart = ((latestCount / days_done) >= lastMinimale) ?
        '<span>' + Math.round(latestCount / days_done).toLocaleString('fr') + '</span>' :
        '<span style="color: red" class="animated flash infinite">' + Math.round(latestCount / days_done).toLocaleString('fr') + '</span>';

    // calcul pour afficher les s tatitisques des 7 derniers jours
    var evolution_semaine = chartData.slice(chartData.length - 7, chartData.length)
        .map(obj => '<div class="text-center">' + moment(obj["date"]).format("DD/MM/YYYY") +
            '&emsp;:&emsp;<b>' + obj["evolution"].toLocaleString('fr') + '</b></div>');

    var lastUpdate_msg = (moment(lastUpdate).isValid()) ?
        'dernière mise à jour le ' + moment(lastUpdate).format('LLLL') :
        'date de mise à jour non définie';

    // statisques pour modal
    var contentStats = '<dl class="row">';
    contentStats += '<dt class="col-sm-3 text-right"><b>' +
        chartData.length + '</b></dt><dd class="col-sm-9">jours écoulés depuis le 13/06/2019 inclus</dd>';
    contentStats += '<dt class="col-sm-3 border-top"></dt><dd class="col-sm-9 border-top"></dd>';
    contentStats += '<dt class="col-sm-3 text-right"><b>' + cumul.toLocaleString('fr') +
        '</b></dt><dd class="col-sm-9">minimum requis total</dd>';
    contentStats += '<dt class="col-sm-3 text-right"><b>' + latestCount.toLocaleString('fr') +
        '</b></dt><dd class="col-sm-9">soutiens publiés</dd>';

    if (cumul > latestCount)
        contentStats += '<dt class="col-sm-3 text-right"><span style="color: red" class="animated flash infinite"><b>' +
        (cumul - latestCount).toLocaleString('fr') + '</b></span></dt><dd class="col-sm-9">soutiens manquants</dd>';

    contentStats += '<dt class="col-sm-3 border-top"></dt><dd class="col-sm-9 border-top"></dd>';
    contentStats += '<dt class="col-sm-3 text-right"><b>' + perDay_required.toLocaleString('fr') +
        '</b></dt><dd class="col-sm-9">minimum de soutiens requis par jour</dd>';
    contentStats += '<dt class="col-sm-3 text-right"><b>' + averagePerDay_sinceStart +
        '</b></dt><dd class="col-sm-9">moyenne journali&egrave;re depuis le 13/06/2019</dd>';
    contentStats += '<dt class="col-sm-3 text-right"><b>' + average_last30Days.toLocaleString('fr') +
        '</b></dt><dd class="col-sm-9">moyenne journali&egrave;re des 30 derniers jours</dd>';
    contentStats += '<dt class="col-sm-3 text-right"><b>' + average_last14Days.toLocaleString('fr') +
        '</b></dt><dd class="col-sm-9">moyenne journali&egrave;re des 14 derniers jours</dd>';
    contentStats += '<dt class="col-sm-3 text-right"><b>' + average_last7Days.toLocaleString('fr') +
        '</b></dt><dd class="col-sm-9">moyenne journali&egrave;re des 7 derniers jours</dd>';
    contentStats += '<dt class="col-sm-3 border-top"></dt><dd class="col-sm-9 border-top"></dd>';
    contentStats += '</dl>';
    contentStats += '<div>&Eacute;volution sur les 7 derniers jours</div>';
    contentStats += evolution_semaine.join('\n');

    $('#statsModalLabel').text('Statistiques au ' + moment(lastUpdate).format("DD/MM/YYYY"))
    $('#statsDiv').html(contentStats);

    document.getElementById('lastUpdate-span').innerHTML = lastUpdate_msg;

    var chartsTitle_msg = 'Evolution des soutiens au ' + moment(lastUpdate).format('LL') + '<span class="badge badge-secondary ml-4"> + ' +
        chartData[chartData.length - 1].evolution.toLocaleString('fr') + '</span>';

    document.getElementById('charts-title').innerHTML = (moment(lastUpdate).isValid()) ? chartsTitle_msg : "Evolution des soutiens au ?";

    var contentDropdown = (isMobile) ? '' :
        `<select id="selectCharts" class="form-control form-control-sm" style="width: 200px">
        <option value="evolution">Voir l'&eacute;volution</option>
        <option value="tendance">Moyennes journalières</option>
        <option value="geoDpt">Nuage de points (dpts)</option>
        <option value="geoVille">Nuage de points (villes)</option>
        </select>`;

    document.getElementById('tendance-dropdown').innerHTML = contentDropdown;

    document.getElementById('objectif-info').innerHTML = '<b>' + requiredTotal.toLocaleString('fr') + ' soutiens !</b><br>' +
        '<span class="text-justify">rappel de l\'objectif à atteindre pour le ' + dateEnd.format('LL') + '**</span>';

    var htmlRetard = '<i><b>' + lastMinimale.toLocaleString('fr') + ' soutiens !!!</b><br>';
    htmlRetard += '<span class="text-justify"> c\'est le nombre <b>minimum</b> à faire <b>par jour ';
    htmlRetard += 'jusqu\'au 12 mars 2020</b>** pour espérer atteindre l\'ojectif des ';
    htmlRetard += requiredTotal.toLocaleString('fr') + ' soutiens nécessaires<small><sup> (1)</sup></small>.</span></i>';

    document.getElementById('moyMinimale-info').innerHTML = htmlRetard;


    if (!isMobile)
        document.getElementById('selectCharts').onchange = (ev) => {
            if (ev.target.value === "evolution") {
                if(typeof document.getElementById("chartdiv").data === 'undefined') { //initialize plot si abscent
                    drawChart(config)
                }

                document.getElementById('charts-title').innerHTML = (moment(lastUpdate).isValid()) ? chartsTitle_msg : "Evolution des soutiens au ?";
                $('#chartTendance-div').hide();
                $('#tendance-btn').hide()
                $('#min-btn').hide()
                $('#date-btn').hide()
                $('#chartTendance-reset').hide();
                $('#chartGeoVille-div').hide();
                $('#chartGeoDpt-div').hide();
                $('#chartdiv').fadeIn();
                $('#projection-btn').fadeIn();
                $('#prev-moyenne').fadeIn();
                $('#prev-remove').fadeIn();
            } else if (ev.target.value === "tendance") {
                document.getElementById('charts-title').innerHTML = (moment(lastUpdate).isValid()) ?
                    "Moyennes journalières au " + moment(lastUpdate).format('LL') : "Moyennes journalières au ?";
                $('#prev-moyenne').hide();
                $('#prev-remove').hide();
                $('#chartdiv').hide();
                $('#projection-btn').hide();
                $('#date-btn').hide()
                $('#chartGeoVille-div').hide();
                $('#chartGeoDpt-div').hide();
                $('#chartTendance-div').show();
                document.getElementById('chartTendance-div').style.height = "70vh";
                chartTendance(config);
                $('#tendance-btn').fadeIn()
                $('#min-btn').fadeIn();
                $('#chartTendance-reset').fadeIn();
            } else if (ev.target.value === "geoVille") {
                document.getElementById('charts-title').innerHTML = ""
                $('#chartTendance-div').hide();
                $('#tendance-btn').hide()
                $('#min-btn').hide()
                $('#chartTendance-reset').hide();
                $('#prev-moyenne').hide();
                $('#prev-remove').hide();
                $('#chartdiv').hide();
                $('#projection-btn').hide();
                $('#chartGeoDpt-div').hide();
                $('#chartGeoVille-div').fadeIn();
                $('#date-btn').fadeIn()
                document.getElementById('chartGeoVille-div').style.height = "70vh";
                chartGeographique(true, config);
            } else if (ev.target.value === "geoDpt") {
                document.getElementById('charts-title').innerHTML = ""
                $('#chartTendance-div').hide();
                $('#tendance-btn').hide()
                $('#min-btn').hide()
                $('#chartTendance-reset').hide();
                $('#prev-moyenne').hide();
                $('#prev-remove').hide();
                $('#chartdiv').hide();
                $('#projection-btn').hide();
                $('#chartGeoVille-div').hide();
                $('#chartGeoDpt-div').fadeIn();
                $('#date-btn').fadeIn()
                document.getElementById('chartGeoDpt-div').style.height = "70vh";
                chartGeographique(false, config);
            }

            window.history.pushState('', '', '?g='+$("select#selectCharts").prop('selectedIndex'));
        };

    // optionsChart pour plotly.js
    var optionsChart = {
        responsive: true,
        locale: 'fr'
    };

    document.getElementById('chartCSV-btn').onclick = function () {
        var dataExport = [];
        chartData.forEach(item => {
            dataExport.push([moment(item["date"]).format("LL"), item["visits"], item["cumul_theorique"],
                item["evolution"], item["difference"], item["moyenne_mobile"], item["moyenne_minimale"]
            ])
        })
        dataExport.unshift(["Date", "Soutiens publiés", "Soutiens nécessaires", "Evolution", "Différence", "Moyenne journalière", "Moyenne minimale"]);
        exportToCsv("données-graphiques.csv", dataExport);
    };

    var config = {
        chartData: chartData,
        latestCount: latestCount,
        minimumNeeded: cumul,
        dataSerie_officiel: dataSerie_officiel,
        days_complete: days_complete,
        days_done: days_done,
        days_remaining: days_remaining,
        contentStats: contentStats,
        optionsChart: optionsChart,
        lastUpdate: lastUpdate,
        lastMinimale: lastMinimale,
        lastMoyenne: lastMoyenne,
        average_last30Days: average_last30Days,
        average_last14Days: average_last14Days
    };

    return config;
}

function drawGauge(config) {
    var counter = config.latestCount,
        needed = config.minimumNeeded;

    var bar100 = document.getElementById("bar100");
    var barPercent = document.getElementById("barPercent");
    var barDiff = document.getElementById("diff-bar");

    if (counter < needed) {
        var diff = needed - counter;
        var counterPercent = Math.round((counter / needed) * 100)
        var diffPercent = Math.round((diff / needed) * 100);
        diffPercent = (counterPercent + diffPercent) > 100 ? 100 - counterPercent : diffPercent;
        // console.log(counterPercent + diffPercent);

        bar100.style.backgroundColor = "#E59165";
        bar100.children[0].innerHTML = "Nécessaires";
        bar100.children[1].innerHTML = needed.toLocaleString('fr');
        bar100.style.width = "100%";

        barPercent.style.backgroundColor = "#67B7DC";
        barPercent.children[0].innerHTML = (!isMobile && !isTablet) ? "Publiés" : "";
        barPercent.children[1].innerHTML = counter.toLocaleString('fr');
        barPercent.style.width = counterPercent + '%';

        barDiff.style.backgroundColor = "#AB0055";
        barDiff.children[0].innerHTML = (!isMobile && !isTablet) ? "Manquants" : "";
        barDiff.children[1].innerHTML = diff.toLocaleString('fr');
        barDiff.style.width = diffPercent + '%';
        if ((counterPercent + diffPercent) >= 100)
            barDiff.style.marginLeft = "-5px";
    } else {
        var diff = counter - needed;
        var neededPercent = Math.floor((needed / counter) * 100);
        var diffPercent = Math.floor((diff / counter) * 100);

        bar100.style.backgroundColor = "#67B7DC";
        bar100.children[0].innerHTML = "Publiés";
        bar100.children[1].innerHTML = counter.toLocaleString('fr');
        bar100.style.width = "100%";

        barPercent.style.backgroundColor = "#E59165";
        barPercent.children[0].innerHTML = (!isMobile && !isTablet) ? "Nécessaires" : "";
        barPercent.children[1].innerHTML = needed.toLocaleString('fr');
        barPercent.style.width = neededPercent + '%';

        barDiff.style.backgroundColor = "#00AA7F";
        barDiff.children[0].innerHTML = (!isMobile && !isTablet) ? '"Bonus"' : "";
        barDiff.children[1].innerHTML = diff.toLocaleString('fr');
        barDiff.style.width = diffPercent + '%';
    }
    $(".scoreLabel").show();
    setTimeout(() => {
        // $('#header').load('navigation.html');
    }, 100);
}

function drawChart(config) {
    $('#chartdiv').fadeTo(100, 0);
    var dataVisits = config.chartData.filter((item, i) => i > 4),
        date_max = config.chartData.map(item => item["date"])[config.chartData.length - 1],
        dataVisits_max = dataVisits.map(item => item["visits"])[dataVisits.length - 1],
        cumulTheorique_max = config.chartData.map(item => item["cumul_theorique"])[config.chartData.length - 1];


    var trace1 = {
        x: config.chartData.map(item => item["date"]),
        y: config.chartData.map(item => item["cumul_theorique"]),
        name: "Soutiens nécessaires",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#e59165',
            width: 3
        },
        text: config.chartData.map(item => item["cumul_theorique"]).map((item, i) => (i === config.chartData.length - 1) ? item.toLocaleString() + " " : ""),
        textposition: 'left',
        textfont: {
            family: 'sans serif',
            size: 18,
            color: '#e59165'
        },
        hovertemplate: "<b>Soutiens nécessaires</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    };

    var trace2 = {
        x: dataVisits.map(item => item["date"]),
        y: dataVisits.map(item => item["visits"]),
        name: "Soutiens publiés",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#67B7DC',
            width: 3
        },
        text: dataVisits.map(item => item["visits"]).map((item, i) => (i === dataVisits.length - 1) ? item.toLocaleString() + " " : ""),
        textposition: 'left top',
        textfont: {
            family: 'sans serif',
            size: 18,
            color: '#67B7DC'
        },
        hovertemplate: "<b>Soutiens publiés</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    };

    var trace3 = {
        x: config.dataSerie_officiel.map(item => item["date"]),
        y: config.dataSerie_officiel.map(item => item["comptage_off"]),
        name: "Comptage officiel (Conseil Constitutionnel)",
        type: 'scatter',
        mode: 'markers+text',
        marker: {
            color: '#808000',
            size: 12
        },
        text: config.dataSerie_officiel.map(item => item["comptage_off"].toLocaleString()),
        textposition: 'left center',
        hovertemplate: "<b>Comptage officiel</b><br>" +
            "%{text}<br>" +
            "<extra></extra>"
    };

    var trace4 = {
        x: [date_max, date_max],
        y: [dataVisits_max, cumulTheorique_max],
        ids: 'special',
        name: "Soutiens manquants",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: 'AB0055',
            dash: 'dashdot',
            size: 12
        },
        hovertemplate: "<b>Manquants</b><br>" +
            (cumulTheorique_max - dataVisits_max).toLocaleString('fr') + "<br>" +
            "<extra></extra>"
    };

    var layout = {
        font: {
            size: 18
        },
        margin: {
            l: 80,
            r: 20,
            t: 25,
            pad: 4
        },
        yaxis: {
            automargin: true,
            tickformat: (",.0f"),
            tickfont: {
                color: 'rgb(38, 102, 148)',
                size: 14
            }
        },
        legend: {
            xanchor: "auto",
            yanchor: "top",
            orientation: "h"
        }
    };

    Plotly.newPlot('chartdiv', [trace1, trace2, trace3], layout, config.optionsChart)
        .then(function () {
            $('#chartdiv').fadeTo(500, 1);
            $("#chartdiv").find('g.legendpoints').find('text').css('visibility', 'hidden');
            document.getElementById('projection-select').value = "choice";
            document.getElementById("projection-info").innerHTML = '<b>' +
                Math.round(config["average_last14Days"] * config.days_remaining + config.latestCount).toLocaleString('fr') + ' soutiens !!</b><br>' +
                '<span class="text-justify"> c`est l\'estimation pour la même date, à partir de l\'évolution <b>des 14 derniers jours</b></span>';
        })

    function removeSeries() {
        document.getElementById('moyMinimale-info').classList.remove("animated", "flash", "infinite");
        drawChart(config);
    }

    document.getElementById('projection-select').onchange = function () {
        (this.value !== "choice") ? addSeries(config): drawChart(config);
    };
    document.getElementById('prev-remove').onclick = removeSeries;
}


// ============ projections ============================ //
function addSeries(config) {
    $('#chartdiv').fadeTo("fast", 0.1);
    var selectedValue = document.getElementById('projection-select').value;
    var select = document.getElementById('projection-select');
    var selectedText = select.options[select.selectedIndex].text;

    document.getElementById("projection-info").innerHTML = '<b>' +
        Math.round(config[selectedValue] * config.days_remaining + config.latestCount).toLocaleString('fr') +
        ' soutiens !!</b><br>' + '<span class="text-justify"> c`est l\'estimation pour la même date, à partir de l\'évolution <b>' + selectedText + '</b>.</span>';

    var config = config;
    var dataVisits = config.chartData.filter((item, i) => i > 4);

    var latestCount = config.latestCount,
        projection = config.latestCount,
        minimumNeeded = config.minimumNeeded,
        lastMoyenne = config[selectedValue],
        lastMinimale = config.lastMinimale,
        days = 1,
        data_projection = [];

    for (var i = 0; i < config.days_remaining + 1; i++) {
        var dateStr = moment(config.lastUpdate).add(i, 'days').format("YYYY-MM-DD");
        var obj = {};
        obj["date"] = dateStr;
        obj["prevision_needed"] = minimumNeeded;
        obj["prevision_moyenne"] = latestCount;
        obj["prevision_minimale"] = projection;
        data_projection.push(obj);
        minimumNeeded += perDay_required;
        latestCount += lastMoyenne;
        projection += lastMinimale;
    }

    var trace1 = {
        x: config.chartData.map(item => item["date"]),
        y: config.chartData.map(item => item["cumul_theorique"]),
        name: "Soutiens nécessaires",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#e59165',
            width: 5
        },
        text: config.chartData.map(item => item["cumul_theorique"]).map((item, i) => (i === config.chartData.length - 1) ? item.toLocaleString() + " " : ""),
        textposition: 'top',
        textfont: {
            family: 'sans serif',
            size: 18,
            color: '#e59165'
        },
        hovertemplate: "<b>Soutiens nécessaires</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    };

    var trace2 = {
        x: dataVisits.map(item => item["date"]),
        y: dataVisits.map(item => item["visits"]),
        name: "Soutiens publiés",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#67B7DC',
            width: 5
        },
        text: dataVisits.map(item => item["visits"]).map((item, i) => (i === dataVisits.length - 1) ? item.toLocaleString() + " " : ""),
        textposition: 'top',
        textfont: {
            family: 'sans serif',
            size: 18,
            color: '#67B7DC'
        },
        hovertemplate: "<b>Soutiens publiés</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    };

    var trace4 = {
        x: data_projection.map(item => item["date"]),
        y: data_projection.map(item => item["prevision_needed"]),
        name: "Projection soutiens nécessaires",
        fill: 'tozeroy',
        fillcolor: 'rgba(238, 153, 102, 0.05)',
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#e59165',
            dash: 'dot',
            width: 1
        },
        text: data_projection.map(item => item["prevision_needed"]).map((item, i) => (i === data_projection.length - 1) ? "4 717 396 " : ""),
        textposition: 'left',
        textfont: {
            family: 'sans serif',
            size: 16,
            color: '#e59165'
        },
        hovertemplate: "<b>Projection soutiens nécessaires</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    };

    var trace5 = {
        x: data_projection.map(item => item["date"]),
        y: data_projection.map(item => item["prevision_moyenne"]),
        name: "Prévision soutiens publiés",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#67B7DC',
            dash: 'dot',
            width: 4
        },
        text: data_projection.map(item => item["prevision_moyenne"])
            .map((item, i) => (i === data_projection.length - 1) ?
                Math.round(config[selectedValue] * config.days_remaining + config.latestCount).toLocaleString('fr') + " " : ""),
        textposition: 'left',
        textfont: {
            family: 'sans serif',
            size: 16,
            color: '#67B7DC'
        },
        hovertemplate: "<b>Prévision soutiens publiés</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    };

    var trace6 = {
        x: data_projection.map(item => item["date"]),
        y: data_projection.map(item => item["prevision_minimale"]),
        name: "Moyenne minimale à faire : " + config.lastMinimale.toLocaleString('fr') + ' soutiens / jour',
        type: 'scatter',
        mode: 'lines',
        line: {
            color: '#AB0055',
            dash: 'dashdot',
            width: 2
        },
        hovertemplate: "<b>Prévision sur moyenne minimale</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    };

    var layout = {
        font: {
            size: 18
        },
        margin: {
            l: 85,
            r: 10,
            b: 50,
            t: 35,
            pad: 4
        },
        yaxis: {
            automargin: true,
            tickformat: (",.0f"),
            tickfont: {
                color: 'rgb(38, 102, 148)',
                size: 14
            },
        },
        legend: {
            xanchor: "auto",
            yanchor: "top",
            orientation: "h"
        },
        annotations: [{
                showarrow: false,
                text: "<b>PROJECTION</b>",
                font: {
                    size: 30,
                    color: 'rgba(51, 51, 51, 0.7)'
                },
                xref: 'paper',
                yref: 'paper',
                x: 0.2,
                y: 0.85
            },
            {
                showarrow: false,
                text: 'à partir de l\'évolution ' + selectedText,
                font: {
                    size: 16,
                    color: '#3D3D3D'
                },
                xref: 'paper',
                yref: 'paper',
                x: 0.2,
                y: 0.75
            },
            {
                showarrow: false,
                text: "Cliquez sur une légende pour afficher ou masquer une série",
                font: {
                    size: 16,
                    color: '#9E6245'
                },
                xref: 'paper',
                yref: 'paper',
                x: 0.9,
                y: 0.1
            }
        ]
    };

    var frames = []
    var x = data_projection.map(item => item["date"]);
    var y = data_projection.map(item => item["prevision_minimale"]);

    var n = data_projection.length;
    for (var i = 0; i < n; i++) {
        frames[i] = {
            data: [{
                x: [],
                y: []
            }]
        }
        frames[i].data[0].x = x.slice(0, i + 1);
        frames[i].data[0].y = y.slice(0, i + 1);
    }

    Plotly.newPlot('chartdiv', [trace6, trace1], layout, config.optionsChart)
        .then(function () {
            Plotly.addTraces('chartdiv', trace2);
        })
        .then(function () {
            Plotly.addTraces('chartdiv', trace4);
        })
        .then(function () {
            $("#chartdiv").find('g.legendpoints').find('text').css('visibility', 'hidden');
            Plotly.addTraces('chartdiv', trace5);
        })
        .then(function () {
            $('#chartdiv').fadeTo(1000, 1);
            document.getElementById('moyMinimale-info').classList.add("animated", "flash", "infinite");
            document.getElementById('diff-bar').classList.add("animated", "flash", "infinite");
            Plotly.animate('chartdiv', frames, {
                transition: {
                    duration: 0
                },
                frame: {
                    duration: 0,
                    redraw: false
                }
            }).then(function () {
                document.getElementById('moyMinimale-info').classList.remove("animated", "flash", "infinite");
                document.getElementById('diff-bar').classList.remove("animated", "flash", "infinite");
            });
        });
} // fin de addSeries


function chartTendance(config) {
    $('#chartTendance-div').fadeTo("fast", 0);
    var dataEvolution_x = config.chartData.filter(item => item["evolution"]).map(item => item["date"]);
    var dataEvolution_y = config.chartData.map(item => item["evolution"]);

    var trace1 = {
        x: dataEvolution_x,
        y: dataEvolution_y,
        name: "Soutiens publiés journaliers",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#67B7DC',
            // dash: 'dot',
            width: 3
        },
        text: dataEvolution_y.map((item, i) => (i === 0 || i === dataEvolution_y.length - 1) ? item.toLocaleString('fr') : ""),
        textposition: 'top',
        textfont: {
            family: 'sans serif',
            size: 16,
            color: '#67B7DC'
        },
        hovertemplate: "<b>Soutiens publiés journaliers</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    };

    var trace2 = {
        x: config.chartData.map(obj => obj["date"]),
        y: config.chartData.map(obj => obj["moyenne_mobile"]),
        name: "Moyenne journalière",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#0000FF',
            dash: "dot",
            width: 2
        },
        text: config.chartData.map(obj => obj["moyenne_mobile"]).map((item, i) => (i === dataEvolution_y.length - 1) ? item.toLocaleString('fr') : ""),
        textposition: 'top',
        textfont: {
            family: 'sans serif',
            size: 16,
            color: '#0000FF'
        },
        hovertemplate: "<b>Moyenne journalière</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    };

    var trace3 = {
        x: config.chartData.map(obj => obj["date"]),
        y: config.chartData.map(obj => obj["moyenne_minimale"]),
        name: "Moyenne minimale à faire",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#FF0000',
            dash: "dot",
            size: 2
        },
        text: config.chartData
            .map(obj => obj["moyenne_minimale"]).map((item, i) => (i === 0 || i === dataEvolution_y.length - 1) ? item.toLocaleString('fr') : ""),
        textposition: 'top',
        textfont: {
            family: 'sans serif',
            size: 16,
            color: '#FF0000'
        },
        hovertemplate: "<b>Moyenne minimale à faire</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    };

    var trace4 = {
        x: config.chartData.map(obj => obj["date"]),
        y: config.chartData.map(obj => 17217),
        name: "Minimum théorique journalier",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#E59165',
            dash: "dashdot",
            size: 2
        },
        text: config.chartData.map(obj => 17217).map((item, i) => (i === 0) ? item.toLocaleString('fr') : ""),
        textposition: 'top',
        textfont: {
            family: 'sans serif',
            size: 16,
            color: '#E59165'
        },
        hovertemplate: "<b>Moyenne théorique journalière</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    };

    var data = [trace1, trace2, trace3];

    var layout = {
        font: {
            size: 18
        },
        margin: {
            l: 65,
            r: 20,
            b: 50,
            t: 35,
            pad: 4
        },
        yaxis: {
            automargin: true,
            tickformat: ("."),
            tickfont: {
                size: 14
            },
        },
        legend: {
            xanchor: "auto",
            yanchor: "top",
            orientation: "h"
        }
    };

    var chart = Plotly.newPlot('chartTendance-div', data, layout, config.optionsChart).then(function () {
        $('#chartTendance-div').fadeTo(500, 1);
    });
    $("#chartTendance-div").find('g.legendpoints').find('text').css('visibility', 'hidden');

    document.getElementById('min-btn').onclick = function () {
        $('#chartTendance-div').fadeTo('fast', 0);
        var flag = true;
        document.getElementById('chartTendance-div').data.forEach(item => {
            if (item.name === "Minimum théorique journalier")
                flag = false;
        });
        if (flag)
            Plotly.addTraces('chartTendance-div', trace4, [0]);
        $('#chartTendance-div').fadeTo(500, 1);
        $("#chartTendance-div").find('g.legendpoints').find('text').css('visibility', 'hidden');
    }

    document.getElementById('chartTendance-reset').onclick = function () {
        chartTendance(config);
    }
}

// ============ geographie ============================ //
var currentDataTs;
var currentChartGeo = "";
var scatterTraceCache = {"chartGeoVille-div" : {}, "chartGeoDpt-div": {}};
function getScatterTrace(newTS, divID, callback) {
    if(divID in scatterTraceCache && newTS in scatterTraceCache[divID]) {
        callback(scatterTraceCache[divID][newTS])
        return
    }

    reloadDataTS(newTS, function(dpts, communes) {
        var ok_communes = Object.entries(communes).filter(c => c[1].electeurs > 0)
        scatterTraceCache["chartGeoVille-div"][newTS] = {
                x: ok_communes.map(c => c[1].electeurs),
                y: ok_communes.map(c => 100*c[1].soutiens/c[1].electeurs),
                text: ok_communes.map(c => "<b>"+c[1].nom+" ("+c[0]+")</b><br>Nombre de soutiens : "+c[1].soutiens.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")+"<br>"),
                mode: 'markers',
                type: 'scatter',
                marker: { size: 3 },
                orientation: 'h',
                hovertemplate:
                    "%{text}" + //nom commune et nombre soutiens
                    "Nombre d'électeurs : %{x:,d}<br>"+
                    "Taux : %{y:.2f}%" +
                    "<extra></extra>",
                showlegend: false
            };

        var ok_dpts = Object.entries(dpts)
        scatterTraceCache["chartGeoDpt-div"][newTS] = {
                x: ok_dpts.map(c => c[1].electeurs),
                y: ok_dpts.map(c => 100*c[1].soutiens/c[1].electeurs),
                text: ok_dpts.map(c => "<b>"+formatDepartements(c[1].nom)+" ("+c[0]+")</b><br>Nombre de soutiens : "+c[1].soutiens.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")+"<br>"),
                mode: 'markers',
                type: 'scatter',
                marker: { size: 10 },
                orientation: 'h',
                hovertemplate:
                    "%{text}" + //nom dpt et nombre soutiens
                    "Nombre d'électeurs : %{x:,d}<br>"+
                    "Taux : %{y:.2f}%" +
                    "<extra></extra>",
                showlegend: false
            };

        callback(scatterTraceCache[divID][newTS])
    })
}

function chartGeographique(isVille, config) { //charge les charts pour la repartition geographique (villes if isVille, or departements)
    if(isVille) {
        currentChartGeo = "chartGeoVille-div"
    } else{
        currentChartGeo = "chartGeoDpt-div"
    }

    if(typeof document.getElementById(currentChartGeo).data !== 'undefined') { //plot already plotted
        return
    }

    $("#mainloader").show();
    $('select#dataDate').prop('disabled', true);
    $.get(urlADPRipCarte , function(data) { //get a list of available data timestamp
        currentDataTs = fillDataDateSelect(data, $('select#dataDate'));
        getScatterTrace(currentDataTs, currentChartGeo, function(graphTrace) {
            var layout = {
                autosize: true,
                xaxis: {
                    type: 'log',
                    title: {
                        text: 'Nombre d\'électeurs',
                        font: {
                            family: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
                            size: 18,
                        }
                    }
                },
                yaxis: {
                    ticksuffix: ' %',
                    tickprefix: '  ',
                    title: {
                        text: 'Taux de soutiens',
                        font: {
                            family: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
                            size: 18,
                        }
                    }
                },
                shapes: [
                    {
                        type: 'line',
                        xref: 'paper',
                        x0: 0,
                        y0: isVille?10:1,
                        x1: 1,
                        y1: isVille?10:1,
                        line: {
                            color: isVille?'rgb(50, 171, 96)':'rgb(255, 203, 48)',
                            width: 1,
                            dash: 'dashdot'
                        }
                    }
                ],
                title:'Pourcentage de soutiens en fonction du nombre d\'électeurs ('+(isVille?'villes':'départements')+')',
                hovermode: 'closest'
            };

            if(isVille) {
                layout.shapes.push({
                        type: 'line',
                        xref: 'paper',
                        x0: 0,
                        y0: 5,
                        x1: 1,
                        y1: 5,
                        line: {
                            color: 'rgb(255, 203, 48)',
                            width: 1,
                            dash: 'dashdot'
                        }
                });
            }


            $("#mainloader").hide();
            $('select#dataDate').prop('disabled', false);
            Plotly.newPlot(currentChartGeo, [graphTrace], layout, config.optionsChart);

            /*
            //change la taille du point si hover - ne marche pas?
            var thisPlot = document.getElementById(currentChartGeo)
            thisPlot.on('plotly_hover', function(data) {
                var pn='',
                    tn='',
                    colors=[];
                for(var i=0; i < data.points.length; i++){
                    pn = data.points[i].pointNumber;
                    tn = data.points[i].curveNumber;
                    colors = data.points[i].data.marker.color;
                }
                colors[pn] = '#C54C82';
                var update = {'marker':{color: colors, size:2}};
                Plotly.restyle('chartGeoVille-div', update, [tn]);
            })

            thisPlot.on('plotly_unhover', function(data) {
                var pn='',
                    tn='',
                    colors=[];
                for(var i=0; i < data.points.length; i++){
                    pn = data.points[i].pointNumber;
                    tn = data.points[i].curveNumber;
                    colors = data.points[i].data.marker.color;
                }
                colors[pn] = '#00000';
                var update = {'marker':{color: colors, size:2}};
                Plotly.restyle('chartGeoVille-div', update, [tn]);
            })
            */
        })
    })
}
/* INTRO JS */
function setIntro() {
    document.getElementById('guide-btn').onclick = function (e) {

        var offsetHeigth = document.getElementById('chartTendance-div').offsetHeight;
        var intro = introJs();
        var chartTendance = {
            element: `#chartTendance-div`,
            intro: `<p>Cliquez sur les légendes du bas pour afficher ou masquer une série sur le graphique.</p>`,
            position: `top`
        };
        var options = [{
                intro: `<h4>Bienvenue dans la visite guidée.</h4><p>Vous pouvez interrompre la visite à tout moment
                        et y revenir quand vous voulez en cliquant sur "Guide"</p>`
            },
            {
                element: `#chartdiv`,
                intro: `<p>Cliquez sur les légendes du bas pour afficher ou masquer une série sur le graphique.</p>`,
                position: `top`
            },
            {
                element: `#gaugediv`,
                intro: ` <p>Nombre de soutiens publiés à ce jour, retard par rapport aux "soutiens nécessaires"
                et nombre de soutiens restant à valider pour atteindre l'objectif d’environ 4,7 millions de soutiens.</p>`,
                position: 'bottom'
            },
            {
                element: `#previsions-infos`,
                intro: `<p>Si la moyenne journalière des soutiens validés est trop basse, le "seuil minimum requis" d’environ 4,7 millions de soutiens,
                    soit 10% du corps électoral pourrait ne pas être atteint pour le 12/03/2019 date butoir.</p>
                    <p><a href="https://www.referendum.interieur.gouv.fr/" target="_blank" rel="noopener noreferrer" title="referendum.interieur.gouv.fr">En savoir plus...</a></p>
                    <p>La "moyenne minimale" à faire et à tenir pendant la durée des jours restants est actualisée journalièrement à partir de la moyenne
                    réellement effectuée et la "moyenne théorique" requise pour obtenir les 4 717 396 soutiens nécessaires.</p>
                    <p>Projections à partir de plusieurs moyennes du nombre final des soutiens qui pourrait être publiés à la date butoir du 12/03/2020
                    (visibles sur graphiques des moyennes journalières)</p>`,
                position: 'top'
            },
            {
                element: `#selectCharts`,
                intro: `Permet de passer d'un graphique à l'autre`,
                position: 'top'
            }
        ];
        if (offsetHeigth > 0) {
            options.splice(1, 1, chartTendance);
        }

        var optionsTendance =
            intro.setOptions({
                tooltipPosition: 'top',
                nextLabel: 'Suivant',
                prevLabel: 'Retour',
                skipLabel: 'Sortir',
                doneLabel: 'Terminer',
                showProgress: true,
                showStepNumbers: true,
                steps: options
            });
        intro.start();
    }
}

/* HELPERS */
function onStart() {
    if (isMobile) {
        $('#projection-btn').hide();
        $('#tendance-btn').hide();
        $('#chartCSV-btn').hide();
        $('#tendance-infos').show();
        document.getElementById('application-btn').classList.add('mt-2');
        document.getElementById('stats-btn').innerHTML = '<span class="fa fa-stats mr-1" aria-hidden="true">';
    } else if (isTablet) {
        $('#chartCSV-btn').hide();
        $('#projection-btn').show();
        $('#tendance-btn').show();
        document.getElementById('application-btn').classList.add('mt-2');
        // document.getElementById('prev-moyenne').innerHTML = 'Voir projection';
    } else {
        $('#projection-btn').show();
        $('#tendance-btn').show();
        $('#chartCSV-btn').show();
    }

    $('#date-btn').hide();
}

function exportToCsv(filename, rows) {
    var processRow = function (row) {
        var finalVal = '';
        for (var j = 0; j < row.length; j++) {
            var innerValue = row[j] === null ? '' : row[j].toString();
            if (row[j] instanceof Date) {
                innerValue = row[j].toLocaleString();
            };
            var result = innerValue.replace(/"/g, '""');
            if (result.search(/("|,|\n)/g) >= 0)
                result = '"' + result + '"';
            if (j > 0)
                finalVal += ',';
            finalVal += result;
        }
        return finalVal + '\n';
    };

    var csvFile = '';
    for (var i = 0; i < rows.length; i++) {
        csvFile += processRow(rows[i]);
    }

    var blob = new Blob([csvFile], {
        type: 'text/csv;charset=utf-8;'
    });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}
